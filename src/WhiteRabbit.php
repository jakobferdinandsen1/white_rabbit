<?php

class WhiteRabbit {

    public function findMedianLetterInFile($filePath) {
        return array("letter" => $this->findMedianLetter($this->parseFile($filePath), $occurrences), "count" => $occurrences);
    }

    /**
     * Parse the input file for letters.
     * @param string $filePath
     * @return bool|string false if file read not successful, a string containing letters in the file if successful
     */
    private function parseFile($filePath) {
        $result = file_get_contents($filePath);
        if ($result) {
            // Use regex to filter out anything but unicode letters
            $result = preg_replace('/[^\p{L}]/u', '', strtolower($result));
        }
        return $result;
    }

    /**
     * Return the letter whose occurrences are the median.
     * @param string $parsedFile All letters of the file
     * @param int $occurrences A reference to the occurrences variable
     * @return string The median letter
     */
    private function findMedianLetter($parsedFile, &$occurrences) {
        $result = count_chars($parsedFile, 1); // Split into array with character => occurrence count

        asort($result); // Sort the array

        $keys = array_keys($result);
        $medianKey = $keys[count($keys) / 2]; // Find median key

        $occurrences = $result[$medianKey];
        return chr($medianKey);
    }
}