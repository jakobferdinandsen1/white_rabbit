<?php

class WhiteRabbit2 {

    /**
     * return a php array, that contains the amount of each type of coin, required to fulfill the amount.
     * The returned array should use as few coins as possible.
     * The coins available for use is: 1, 2, 5, 10, 20, 50, 100
     * You can assume that $amount will be an int
     */
    public function findCashPayment($amount) {
        // Array to be filled with coins
        $coins = array(
            '100' => 0,
            '50' => 0,
            '20' => 0,
            '10' => 0,
            '5' => 0,
            '2' => 0,
            '1' => 0
        );

        foreach ($coins as $coin => $count) { // Loop through possible coins
            while ($amount >= $coin) { //Add a coin while amount is higher than the coins value.
                $coins[$coin]++;
                $amount = $amount - $coin; // Reduce the amount by the value of the coin added.
            }
        }

        return $coins;
    }
}